package pl.lukaszplac123.weather.application;

import java.io.InputStreamReader;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

import pl.lukaszplac123.console.AppContext;
import pl.lukaszplac123.console.AppType;
import pl.lukaszplac123.console.ContextSwitcher;
import pl.lukaszplac123.console.InterfaceType;

public class WeatherApp implements Observer {
	
	private boolean quitRequest;
	
	
	public WeatherApp() {
		quitRequest = false;
	}
	
	public static void main(String[] args) {
		WeatherApp weatherApp = new WeatherApp();
		AppContext context = new ContextSwitcher();
		AppType appType = context.switchContext(weatherApp, InterfaceType.CONSOLE_GENERAL);
		Scanner inputCommand = new Scanner(new InputStreamReader(System.in));
		String contextPrompt = "general";
		while (!weatherApp.quitRequest){
			 weatherApp.printPrompt(contextPrompt);
			 String[] commands = (inputCommand.nextLine()).split("-");
			 String interpreted = appType.interpret(commands);
			 if (interpreted.equals("-wconsole")){
				 appType = context.switchContext(weatherApp, InterfaceType.CONSOLE_TEXT);
				 contextPrompt = "weather";
				 System.out.println("Weather console running...");
				 continue;
			 } else if (interpreted.equals("-main")){
				 appType = context.switchContext(weatherApp, InterfaceType.CONSOLE_GENERAL);
				 contextPrompt = "general";
				 System.out.println("General console running...");
				 continue;
			 } else if (interpreted.equals("-gui")){
				 appType = context.switchContext(weatherApp, InterfaceType.GUI);
				 contextPrompt = "gui";
				 System.out.println("GUI running...");
				 continue;
			 }
			 System.out.println(interpreted);
		}
		inputCommand.close();
		
	}

	public void update(Observable arg0, Object arg1) {
		quitRequest = true;
	}
	private void printPrompt(String context) {
		System.out.print(context+"> ");
	}
}
