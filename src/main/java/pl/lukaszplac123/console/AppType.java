package pl.lukaszplac123.console;

import java.util.Observable;

import pl.lukaszplac123.console.commands.Command;
import pl.lukaszplac123.console.commands.GuiCommand;

public abstract class  AppType extends Observable {
	public String runVersion(){
		return "WeatherApp -> Version : 0.0.1";
	}
	
	public String quit(){
		setChanged();
		notifyObservers();
		return "Quitting...";
	}
	
	public String gui(){
		Command command = new GuiCommand();
		return command.runCommand();
	}
	
	public abstract String interpret(String[] command);
		
	public abstract String runHelp();

}
