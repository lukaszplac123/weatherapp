package pl.lukaszplac123.console;

public interface WeatherCommands {
	public String getCommand(String command);
	public String getForecastCommand(String command);
	public String getTempCommand(String command);
	public String getWindCommand(String command);
	public String getPressureCommand(String command);
	public String getHumidityCommand(String command);
	public String getPollutionCommand(String command);
	public String goBack();
}
