package pl.lukaszplac123.console;

import pl.lukaszplac123.console.commands.Command;
import pl.lukaszplac123.console.commands.GetHumidityCommand;
import pl.lukaszplac123.console.commands.GetPollutionCommand;
import pl.lukaszplac123.console.commands.GetPressureCommand;
import pl.lukaszplac123.console.commands.GetTempCommand;
import pl.lukaszplac123.console.commands.GetWeatherCommand;
import pl.lukaszplac123.console.commands.GetWeatherForecastCommand;
import pl.lukaszplac123.console.commands.GetWindCommand;
import pl.lukaszplac123.console.commands.HelpCommandWeather;
import pl.lukaszplac123.console.commands.QuitCommandWeather;
import pl.lukaszplac123.console.interpreter.ConsoleWeatherInterpreter;
import pl.lukaszplac123.console.interpreter.Interpreter;
import pl.lukaszplac123.weather.application.WeatherApp;

public class ConsoleWeather extends AppType implements WeatherCommands{
	
	public ConsoleWeather(WeatherApp app) {
		addObserver(app);
	}

	public String runHelp() {
		Command command = new HelpCommandWeather();
		return command.runCommand();
	}

	@Override
	public String interpret(String command[]) {
		Interpreter interpreter = new ConsoleWeatherInterpreter();
		return interpreter.interpret(this, command);
	}

	@Override
	public String getCommand(String city) {
		Command command = new GetWeatherCommand(city);
		return command.runCommand();
	}
	
	@Override
	public String goBack() {
		Command command = new QuitCommandWeather();
		return command.runCommand();
	}
	
	@Override
	public String getTempCommand(String city) {
		Command command = new GetTempCommand(city);
		return command.runCommand();
	}

	@Override
	public String getWindCommand(String city) {
		Command command = new GetWindCommand(city);
		return command.runCommand();
	}

	@Override
	public String getPressureCommand(String city) {
		Command command = new GetPressureCommand(city);
		return command.runCommand();
	}

	@Override
	public String getHumidityCommand(String city) {
		Command command = new GetHumidityCommand(city);
		return command.runCommand();
	}

	@Override
	public String getForecastCommand(String city) {
		Command command = new GetWeatherForecastCommand(city);
		return command.runCommand();
	}

	@Override
	public String getPollutionCommand(String city) {
		Command command = new GetPollutionCommand(city);
		return command.runCommand();
	}

	
}
