package pl.lukaszplac123.console;

import pl.lukaszplac123.weather.application.WeatherApp;

public abstract class AppContext {
	public abstract AppType switchContext(WeatherApp app, InterfaceType type);
}
