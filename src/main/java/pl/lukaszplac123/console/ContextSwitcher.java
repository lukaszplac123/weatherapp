package pl.lukaszplac123.console;

import pl.lukaszplac123.weather.application.WeatherApp;

public class ContextSwitcher extends AppContext{


	@Override
	public AppType switchContext(WeatherApp app, InterfaceType type) {
		switch (type){
		case CONSOLE_GENERAL:{
			return new ConsoleGeneral(app);
		}
		case GUI:
			
			break;
		case CONSOLE_TEXT:
			return new ConsoleWeather(app);
		default:
			break;
		}
		return null;
	}

}
