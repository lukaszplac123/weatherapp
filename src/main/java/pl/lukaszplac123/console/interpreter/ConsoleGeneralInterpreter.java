package pl.lukaszplac123.console.interpreter;

import java.io.IOException;
import java.util.Observer;

import pl.lukaszplac123.console.AppType;
import pl.lukaszplac123.console.ConsoleGeneral;

public class ConsoleGeneralInterpreter implements Interpreter {

	@Override
	public String interpret(AppType appDelivered, String[] command) {	
		ConsoleGeneral app = (ConsoleGeneral) appDelivered;
		switch (command[0]){
		case "help"    : return app.runHelp();
		case "quit"    : return app.quit();
		case "gui"     : return app.gui();
		case "wconsole" : return app.console();
	}
		return "Command not known. Try again...\n";
	}
}
