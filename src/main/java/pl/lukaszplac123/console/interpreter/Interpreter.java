package pl.lukaszplac123.console.interpreter;

import pl.lukaszplac123.console.AppType;

public interface Interpreter {
		public String interpret(AppType app, String[] command);
}
