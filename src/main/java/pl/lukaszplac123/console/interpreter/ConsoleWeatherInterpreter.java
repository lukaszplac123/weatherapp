package pl.lukaszplac123.console.interpreter;

import java.util.function.Function;

import pl.lukaszplac123.console.AppType;
import pl.lukaszplac123.console.ConsoleWeather;

public class ConsoleWeatherInterpreter implements Interpreter {

	@Override
	public String interpret(AppType appDelivered, String[] command) {
		ConsoleWeather app = (ConsoleWeather) appDelivered;
		switch (command[0].trim()){
		case "help"    : return app.runHelp();
		case "quit"    : return app.quit();
		case "gui"     : return app.gui();
		case "main"    : return app.goBack();
		case "getw"    : return app.getCommand(runCommandForSpecifiedCity(command));
		case "gett"    : return app.getTempCommand(runCommandForSpecifiedCity(command));
		case "getp"    : return app.getPressureCommand(runCommandForSpecifiedCity(command));
		case "geth"    : return app.getHumidityCommand(runCommandForSpecifiedCity(command));
		case "getwi"    : return app.getWindCommand(runCommandForSpecifiedCity(command));
		case "getfo"    : return app.getForecastCommand(runCommandForSpecifiedCity(command));
		case "pollut"   : return app.getPollutionCommand(runCommandForSpecifiedCity(command));
		}
		return "Command not known. Try again...\n";
	}

	private String runCommandForSpecifiedCity(String[] command){
		if (!(command.length < 2)){
			 return command[1];
		 }
		 else return "Second argument not present : -[city]";	
	}

}
