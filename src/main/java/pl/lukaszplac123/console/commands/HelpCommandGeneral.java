package pl.lukaszplac123.console.commands;

public class HelpCommandGeneral implements Command {

	public String runCommand() {
		StringBuilder builder = new StringBuilder();
		builder.append("help : list of allowed commands for current console\n");
		builder.append("gui : run graphical mode\n");
		builder.append("wconsole : run console mode\n");
		
		builder.append("quit : quit application\n");
		return builder.toString();
	}

}
