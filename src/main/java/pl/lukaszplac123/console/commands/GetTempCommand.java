package pl.lukaszplac123.console.commands;

import pl.lukaszplac123.requests.Request;
import pl.lukaszplac123.requests.current.RequestGetTemp;

public class GetTempCommand implements Command{
	
	Request request;
	
	public GetTempCommand(String city) {
		request = new RequestGetTemp(city);
	}
	
	@Override
	public String runCommand() {
		return request.obtainSpecificData();
	}

}
