package pl.lukaszplac123.console.commands;

public interface Command {
	public String runCommand();
}
