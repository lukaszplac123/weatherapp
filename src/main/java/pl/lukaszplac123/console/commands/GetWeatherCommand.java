package pl.lukaszplac123.console.commands;

import pl.lukaszplac123.requests.Request;
import pl.lukaszplac123.requests.current.RequestGetWeather;

public class GetWeatherCommand implements Command{
	
	Request request;
	
	public GetWeatherCommand(String city) {
		request = new RequestGetWeather(city);
	}
	
	@Override
	public String runCommand() {
		return request.obtainSpecificData();
	}

}
