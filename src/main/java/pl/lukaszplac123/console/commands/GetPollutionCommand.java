package pl.lukaszplac123.console.commands;

import pl.lukaszplac123.requests.Request;
import pl.lukaszplac123.requests.pollution.RequestGetAirPollution;

public class GetPollutionCommand implements Command {

	Request request;
	
	public GetPollutionCommand(String city) {
		request = new RequestGetAirPollution(city);
	}
	
	@Override
	public String runCommand() {
		return request.obtainSpecificData();
	}

}
