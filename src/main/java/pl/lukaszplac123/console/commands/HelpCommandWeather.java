package pl.lukaszplac123.console.commands;

public class HelpCommandWeather implements Command {

	public String runCommand() {
		StringBuilder builder = new StringBuilder();
		builder.append("help : list of allowed commands for current console\n");
		builder.append("main : go back to general console\n");
		builder.append("getw : get current weather\n"+
					   "	-[city] : get weather by city name\n");
		builder.append("gett : get current temperature\n"+
				   "	-[city] : get temperature by city name\n");
		builder.append("getp : get current pressure\n"+
				   "	-[city] : get pressure by city name\n");
		builder.append("geth : get current humidity\n"+
				   "	-[city] : get humidity by city name\n");
		builder.append("getwi : get current wind\n"+
				   "	-[city] : get wind by city name\n");
		builder.append("getfo : get current forecast\n"+
				   "	-[city] : get forecast by city name\n");
		builder.append("pollut : get current pollution\n"+
				   "	-[city] : get pollution by city name\n");
		builder.append("quit : quit application\n");
		return builder.toString();
	}
	
}
