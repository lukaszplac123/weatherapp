package pl.lukaszplac123.console.commands;

import pl.lukaszplac123.requests.Request;
import pl.lukaszplac123.requests.current.RequestGetHumidity;


public class GetHumidityCommand implements Command{
	
	Request request;
	
	public GetHumidityCommand(String city) {
		request = new RequestGetHumidity(city);
	}
	
	@Override
	public String runCommand() {
		return request.obtainSpecificData();
	}

}
