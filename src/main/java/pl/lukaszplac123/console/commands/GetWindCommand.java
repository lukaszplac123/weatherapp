package pl.lukaszplac123.console.commands;

import pl.lukaszplac123.requests.Request;
import pl.lukaszplac123.requests.current.RequestGetWind;

public class GetWindCommand implements Command{
	
	Request request;
	
	public GetWindCommand(String city) {
		request = new RequestGetWind(city);
	}
	
	@Override
	public String runCommand() {
		return request.obtainSpecificData();
	}

}
