package pl.lukaszplac123.console.commands;

import pl.lukaszplac123.requests.Request;
import pl.lukaszplac123.requests.current.RequestGetPressure;


public class GetPressureCommand implements Command{
	
	Request request;
	
	public GetPressureCommand(String city) {
		request = new RequestGetPressure(city);
	}
	
	@Override
	public String runCommand() {
		return request.obtainSpecificData();
	}

}
