package pl.lukaszplac123.console.commands;

import pl.lukaszplac123.requests.Request;
import pl.lukaszplac123.requests.forecast.RequestGetWeatherForecast;

public class GetWeatherForecastCommand implements Command{
	
	Request request;
	
	public GetWeatherForecastCommand(String city) {
		request = new RequestGetWeatherForecast(city);
	}
	
	@Override
	public String runCommand() {
		return request.obtainSpecificData();
	}

}
