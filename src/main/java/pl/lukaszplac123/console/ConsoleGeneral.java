package pl.lukaszplac123.console;

import pl.lukaszplac123.console.commands.Command;
import pl.lukaszplac123.console.commands.ConsoleCommandGeneral;
import pl.lukaszplac123.console.commands.GuiCommand;
import pl.lukaszplac123.console.commands.HelpCommandGeneral;
import pl.lukaszplac123.console.commands.QuitCommandGeneral;
import pl.lukaszplac123.console.interpreter.ConsoleGeneralInterpreter;
import pl.lukaszplac123.console.interpreter.Interpreter;
import pl.lukaszplac123.weather.application.WeatherApp;

public class ConsoleGeneral extends AppType implements GeneralCommands{
	
	public ConsoleGeneral(WeatherApp app) {
		addObserver(app);
	}

	@Override
	public String runHelp() {
		Command command = new HelpCommandGeneral();
		return command.runCommand();
	}

	@Override
	public String interpret(String[] command) {
		Interpreter interpreter = new ConsoleGeneralInterpreter();
		return interpreter.interpret(this, command);
	}

	@Override
	public String console() {
		Command command = new ConsoleCommandGeneral();
		return command.runCommand();
	}
	
}
