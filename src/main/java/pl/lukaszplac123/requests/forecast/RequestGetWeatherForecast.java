package pl.lukaszplac123.requests.forecast;


import pl.lukaszplac123.data.WeatherConditions;
import pl.lukaszplac123.data.WeatherConditionsForecast;

public class RequestGetWeatherForecast extends RequestWeatherForecast {

	protected WeatherConditionsForecast weatherConditionsForecast;
	
	public RequestGetWeatherForecast(String cityName) {
		super(cityName);
		weatherConditionsForecast = this.executeRequest(new WeatherConditionsForecast());
	}

	@Override
	public String obtainSpecificData() {
		String message = "";
		if (weatherConditionsForecast.getCod() == 200) message = weatherConditionsForecast.toString();
			else {
				message = "Something went wrong. No data received.";
			}
			return message;
	}

	@Override
	public WeatherConditionsForecast getWeatherConditions() {
		if (weatherConditionsForecast.getCod() == 200) return weatherConditionsForecast;
		else return null;
	}
	
}
