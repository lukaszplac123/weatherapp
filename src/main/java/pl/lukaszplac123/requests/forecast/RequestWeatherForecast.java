package pl.lukaszplac123.requests.forecast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import pl.lukaszplac123.data.WeatherConditions;
import pl.lukaszplac123.data.WeatherConditionsForecast;
import pl.lukaszplac123.requests.Request;

public abstract class RequestWeatherForecast extends Request{
	
	private String extension = "forecast/daily?q=";
	private Logger logger = Logger.getLogger(RequestWeatherForecast.class);;
	private HttpGet httpGetRequest;
	
	public RequestWeatherForecast(String cityName) {
		super();
		fullUrl = baseUrl+extension+cityName+apiKey;
		httpGetRequest = new HttpGet(fullUrl);
	}

	protected WeatherConditionsForecast executeRequest(WeatherConditionsForecast weatherConditionsForecast){
		try {
			HttpResponse response = client.execute(httpGetRequest);
			ObjectMapper mapper = new ObjectMapper();
			BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			weatherConditionsForecast = mapper.readValue(reader, WeatherConditionsForecast.class);
		} catch (IOException e) {
			logger.error("Error while getting data:",e);
		} catch (IllegalArgumentException e){
			logger.error("Wrong argument found");
		} finally{
			if (httpGetRequest != null) httpGetRequest.releaseConnection();
		}
		return weatherConditionsForecast;
	}
	


	public abstract WeatherConditionsForecast getWeatherConditions();
}
