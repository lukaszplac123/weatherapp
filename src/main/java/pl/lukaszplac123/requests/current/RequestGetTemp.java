package pl.lukaszplac123.requests.current;


import pl.lukaszplac123.data.WeatherConditions;

public class RequestGetTemp extends RequestWeather {

	
	protected WeatherConditions weatherConditions;
	
	public RequestGetTemp(String cityName) {
		super(cityName);
		weatherConditions = this.executeRequest(new WeatherConditions());
	}

	@Override
	public String obtainSpecificData() {
		String message = "";
		if (weatherConditions.getCod() == 200) message = weatherConditions.printTemperature();
			else {
				message = "Something went wrong. No data received.";
			}
			return message;
	}
	
	@Override
	public WeatherConditions getWeatherConditions() {
		return weatherConditions;
	}
}
