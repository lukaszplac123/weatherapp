package pl.lukaszplac123.requests.current;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpGet;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import pl.lukaszplac123.data.WeatherConditions;
import pl.lukaszplac123.requests.Request;

public abstract class RequestWeather extends Request {
	
	protected final String baseUrl = "http://api.openweathermap.org/data/2.5/weather?q=";
	protected final String apiKey = "&APPID=de512e407867ef7e93c96ade70540f10";
	protected Logger logger = Logger.getLogger(RequestGetWeather.class);;
	protected HttpGet httpGetRequest;
	public RequestWeather(String cityName) {
		fullUrl = baseUrl+cityName+apiKey;	
		httpGetRequest = new HttpGet(fullUrl);
	}

	protected WeatherConditions executeRequest(WeatherConditions weatherConditions){
		try {	
			HttpResponse response = client.execute(httpGetRequest);
			ObjectMapper mapper = new ObjectMapper();
			BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			weatherConditions = mapper.readValue(reader, WeatherConditions.class);
		} catch (IOException e) {
			logger.error("Error while getting data:",e);
		} catch (IllegalArgumentException e){
			logger.error("Wrong argument found");
		} finally{
			if (httpGetRequest != null) httpGetRequest.releaseConnection();
		}
		return weatherConditions;
	}
	
	public abstract String obtainSpecificData();
	
	public abstract WeatherConditions getWeatherConditions();
}
