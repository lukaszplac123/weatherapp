package pl.lukaszplac123.requests.current;


import pl.lukaszplac123.data.WeatherConditions;

public class RequestGetHumidity extends RequestWeather {

	
	protected WeatherConditions weatherConditions;
	
	public RequestGetHumidity(String cityName) {
		super(cityName);
		weatherConditions = this.executeRequest(new WeatherConditions());
	}

	@Override
	public String obtainSpecificData() {
		String message = "";
		if (weatherConditions.getCod() == 200) message = weatherConditions.printHumidity();
			else {
				message = "Something went wrong. No data received.";
			}
			return message;
	}

	@Override
	public WeatherConditions getWeatherConditions() {
		return weatherConditions;
	}
}
