package pl.lukaszplac123.requests.pollution;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import pl.lukaszplac123.data.AirConditions;
import pl.lukaszplac123.data.WeatherConditions;
import pl.lukaszplac123.data.WeatherConditionsForecast;
import pl.lukaszplac123.requests.Request;

public abstract class RequestAirPollution extends Request{
	
	private String extension = "?location=";
	private Logger logger = Logger.getLogger(RequestAirPollution.class);;
	private HttpGet httpGetRequest;
	
	public RequestAirPollution(String cityName) {
		super();
		fullUrl = baseUrlAir+extension+cityName+apiKeyAir;
		httpGetRequest = new HttpGet(fullUrl);
	}

	protected AirConditions executeRequest(AirConditions airConditions){
		try {
			HttpResponse response = client.execute(httpGetRequest);
			ObjectMapper mapper = new ObjectMapper();
			BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			airConditions = mapper.readValue(reader, AirConditions.class);
		} catch (IOException e) {
			logger.error("Error while getting data:",e);
		} catch (IllegalArgumentException e){
			logger.error("Wrong argument found");
		} finally{
			if (httpGetRequest != null) httpGetRequest.releaseConnection();
		}
		return airConditions;
	}
	


	public abstract AirConditions getAirConditions();
}
