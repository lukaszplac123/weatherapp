package pl.lukaszplac123.requests.pollution;


import pl.lukaszplac123.data.AirConditions;
import pl.lukaszplac123.data.WeatherConditions;
import pl.lukaszplac123.data.WeatherConditionsForecast;

public class RequestGetAirPollution extends RequestAirPollution {

	protected AirConditions airConditions;
	
	public RequestGetAirPollution(String cityName) {
		super(cityName);
		airConditions = this.executeRequest(new AirConditions());
	}

	@Override
	public String obtainSpecificData() {
		String message = "";
		if (airConditions.isData_valid() == true) message = airConditions.toString();
			else {
				message = "Something went wrong. No data received.";
			}
			return message;
	}

	@Override
	public AirConditions getAirConditions() {
		if (airConditions.isData_valid() == true) return airConditions;
		return null;
	}
	
}
