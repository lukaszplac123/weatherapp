package pl.lukaszplac123.requests;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

public abstract class Request {
	protected final String baseUrl = "http://api.openweathermap.org/data/2.5/";
	protected final String baseUrlAir = "https://api.breezometer.com/baqi/";
	protected final String apiKey = "&APPID=de512e407867ef7e93c96ade70540f10";
	protected final String apiKeyAir = "&key=74ce1f9c1b3d4947afa425cc5b2b0aa8";
	protected HttpClient client;
	protected String fullUrl;
	public abstract String obtainSpecificData();
	
	public Request(){
		client = HttpClientBuilder.create().build();	
	}
}
