package pl.lukaszplac123.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class AirConditions {

	private String datetime;
	private String country_name;
	private int breezometer_aqi;
	private String breezometer_color;
	private String breezometer_description;
	private int country_aqi;
	private String country_aqi_prefix;
	private String country_color;
	private String country_description;
	private boolean data_valid;
	private boolean key_valid;
	private RandomRecommendations random_recommendations;
	private String dominant_pollutant_canonical_name;
	private String dominant_pollutant_description;
	private DominantPollutantText dominant_pollutant_text;	

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getCountry_name() {
		return country_name;
	}

	public void setCountry_name(String country_name) {
		this.country_name = country_name;
	}

	public int getBreezometer_aqi() {
		return breezometer_aqi;
	}

	public void setBreezometer_aqi(int breezometer_aqi) {
		this.breezometer_aqi = breezometer_aqi;
	}

	public String getBreezometer_color() {
		return breezometer_color;
	}

	public void setBreezometer_color(String breezometer_color) {
		this.breezometer_color = breezometer_color;
	}

	public String getBreezometer_description() {
		return breezometer_description;
	}

	public void setBreezometer_description(String breezometer_description) {
		this.breezometer_description = breezometer_description;
	}

	public int getCountry_aqi() {
		return country_aqi;
	}

	public void setCountry_aqi(int country_aqi) {
		this.country_aqi = country_aqi;
	}

	public String getCountry_aqi_prefix() {
		return country_aqi_prefix;
	}

	public void setCountry_aqi_prefix(String country_aqi_prefix) {
		this.country_aqi_prefix = country_aqi_prefix;
	}

	public String getCountry_color() {
		return country_color;
	}

	public void setCountry_color(String country_color) {
		this.country_color = country_color;
	}

	public String getCountry_description() {
		return country_description;
	}

	public void setCountry_description(String country_description) {
		this.country_description = country_description;
	}

	public boolean isData_valid() {
		return data_valid;
	}

	public void setData_valid(boolean data_valid) {
		this.data_valid = data_valid;
	}

	public boolean isKey_valid() {
		return key_valid;
	}

	public void setKey_valid(boolean key_valid) {
		this.key_valid = key_valid;
	}

	public RandomRecommendations getRandom_recommendations() {
		return random_recommendations;
	}

	public void setRandom_recommendations(RandomRecommendations random_recommendations) {
		this.random_recommendations = random_recommendations;
	}

	public String getDominant_pollutant_canonical_name() {
		return dominant_pollutant_canonical_name;
	}

	public void setDominant_pollutant_canonical_name(String dominant_pollutant_canonical_name) {
		this.dominant_pollutant_canonical_name = dominant_pollutant_canonical_name;
	}

	public String getDominant_pollutant_description() {
		return dominant_pollutant_description;
	}

	public void setDominant_pollutant_description(String dominant_pollutant_description) {
		this.dominant_pollutant_description = dominant_pollutant_description;
	}

	public DominantPollutantText getDominant_pollutant_text() {
		return dominant_pollutant_text;
	}

	public void setDominant_pollutant_text(DominantPollutantText dominant_pollutant_text) {
		this.dominant_pollutant_text = dominant_pollutant_text;
	}

	public static class RandomRecommendations{
		String children;
		String sport;
		String health;
		String inside;
		String outside;
		public String getChildren() {
			return children;
		}
		public void setChildren(String children) {
			this.children = children;
		}
		public String getSport() {
			return sport;
		}
		public void setSport(String sport) {
			this.sport = sport;
		}
		public String getHealth() {
			return health;
		}
		public void setHealth(String health) {
			this.health = health;
		}
		public String getInside() {
			return inside;
		}
		public void setInside(String inside) {
			this.inside = inside;
		}
		public String getOutside() {
			return outside;
		}
		public void setOutside(String outside) {
			this.outside = outside;
		}
		
	}
	
	public static class DominantPollutantText{
		String main;
		String effects;
		String causes;
		public String getMain() {
			return main;
		}
		public void setMain(String main) {
			this.main = main;
		}
		public String getEffects() {
			return effects;
		}
		public void setEffects(String effects) {
			this.effects = effects;
		}
		public String getCauses() {
			return causes;
		}
		public void setCauses(String causes) {
			this.causes = causes;
		}
		
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("\n");
		buffer.append("Country : " + this.getCountry_name()).append("\n");
		buffer.append("AirQuality : " + this.getBreezometer_description()).append("\n");
		buffer.append("Dominant pollutant : " + this.getDominant_pollutant_canonical_name()).append("\n");
		buffer.append(this.getDominant_pollutant_description()).append("\n");
		buffer.append("Recommendation(children) : " + this.getRandom_recommendations().getChildren()).append("\n");
		buffer.append("Recommendation(sport) : " + this.getRandom_recommendations().getSport()).append("\n");
		buffer.append("Recommendation(health) : " + this.getRandom_recommendations().getHealth()).append("\n");
		buffer.append("Recommendation(inside) : " + this.getRandom_recommendations().getInside()).append("\n");
		buffer.append("Recommendation(outside) : " + this.getRandom_recommendations().getOutside()).append("\n");
	return buffer.toString();	
	}
}
