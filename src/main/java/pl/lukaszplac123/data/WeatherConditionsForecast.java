package pl.lukaszplac123.data;

import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonValueInstantiator;

@JsonIgnoreProperties(ignoreUnknown=true)
public class WeatherConditionsForecast {
	
	private City city;
	private int cod;
	private double message;
	private int cnt;
	private Forecast[] list;
	
	
	
	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public int getCod() {
		return cod;
	}

	public void setCod(int cod) {
		this.cod = cod;
	}

	public double getMessage() {
		return message;
	}

	public void setMessage(double message) {
		this.message = message;
	}

	public int getCnt() {
		return cnt;
	}

	public void setCnt(int cnt) {
		this.cnt = cnt;
	}

	public Forecast[] getList() {
		return list;
	}

	public void setList(Forecast[] list) {
		this.list = list;
	}
	
	@JsonIgnoreProperties(ignoreUnknown=true)
	public static class City{
		int id;
		String name;
		Coord coord;
		String country;
		int population;
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public Coord getCoord() {
			return coord;
		}
		public void setCoord(Coord coord) {
			this.coord = coord;
		}
		public String getCountry() {
			return country;
		}
		public void setCountry(String country) {
			this.country = country;
		}
		public int getPopulation() {
			return population;
		}
		public void setPopulation(int population) {
			this.population = population;
		}
		
	
	}
	
	@JsonIgnoreProperties(ignoreUnknown=true)
	public static class Coord{
		double lon;
		double lat;
		public double getLon() {
			return lon;
		}
		public void setLon(double lon) {
			this.lon = lon;
		}
		public double getLat() {
			return lat;
		}
		public void setLat(double lat) {
			this.lat = lat;
		}
		
	}
	
	@JsonIgnoreProperties(ignoreUnknown=true)
	public static class Forecast{
		long dt;
		Temp temp;
		double pressure;
		int humidity;
		Weather[] weather;
		double speed;
		int deg;
		int clouds;
		double rain;
		double snow;
		
		public double getSnow() {
			return snow;
		}
		public void setSnow(double snow) {
			this.snow = snow;
		}
		public long getDt() {
			return dt;
		}
		public void setDt(long dt) {
			this.dt = dt;
		}
		public Temp getTemp() {
			return temp;
		}
		public void setTemp(Temp temp) {
			this.temp = temp;
		}
		public double getPressure() {
			return pressure;
		}
		public void setPressure(double pressure) {
			this.pressure = pressure;
		}
		public int getHumidity() {
			return humidity;
		}
		public void setHumidity(int humidity) {
			this.humidity = humidity;
		}
		public Weather[] getWeather() {
			return weather;
		}
		public void setWeather(Weather[] weather) {
			this.weather = weather;
		}
		public double getSpeed() {
			return speed;
		}
		public void setSpeed(double speed) {
			this.speed = speed;
		}
		public int getDeg() {
			return deg;
		}
		public void setDeg(int deg) {
			this.deg = deg;
		}
		public int getClouds() {
			return clouds;
		}
		public void setClouds(int clouds) {
			this.clouds = clouds;
		}
		public double getRain() {
			return rain;
		}
		public void setRain(double rain) {
			this.rain = rain;
		}	
	}
	
	@JsonIgnoreProperties(ignoreUnknown=true)
	public static class Temp{
		double day;
		double min;
		double max;
		double night;
		double eve;
		double morn;
		public double getDay() {
			return day;
		}
		public void setDay(double day) {
			this.day = day;
		}
		public double getMin() {
			return min;
		}
		public void setMin(double min) {
			this.min = min;
		}
		public double getMax() {
			return max;
		}
		public void setMax(double max) {
			this.max = max;
		}
		public double getNight() {
			return night;
		}
		public void setNight(double night) {
			this.night = night;
		}
		public double getEve() {
			return eve;
		}
		public void setEve(double eve) {
			this.eve = eve;
		}
		public double getMorn() {
			return morn;
		}
		public void setMorn(double morn) {
			this.morn = morn;
		}
		
	}
	
	@JsonIgnoreProperties(ignoreUnknown=true)
	public static class Weather{
		int id;
		String main;
		String description;
		String icon;
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getMain() {
			return main;
		}
		public void setMain(String main) {
			this.main = main;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String getIcon() {
			return icon;
		}
		public void setIcon(String icon) {
			this.icon = icon;
		}
		
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();

		buffer.append("\n");
		buffer.append("City : " + this.getCity().name).append("\n");
		buffer.append("City longtitude: " + this.getCity().coord.lon).append("\n");
		buffer.append("City latitude: " + this.getCity().coord.lat).append("\n");
		int length = this.getList().length;
		for (int i = 0 ; i < length ; i++){
			
			Instant date = Instant.ofEpochSecond(this.getList()[i].dt);
			double celcius = this.getList()[i].temp.day - 273.15;
			double celciusMax = this.getList()[i].temp.max - 273.15;
			double celciusMin = this.getList()[i].temp.min - 273.15;
			
			buffer.append("Day : " + date.toString()).append("\n");
			buffer.append("Temperature : " + celcius).append(" stC").append("\n");
			buffer.append("Temperature max : " + celciusMax).append(" stC").append("\n");
			buffer.append("Temperature min : " + celciusMin).append(" stC").append("\n");
			buffer.append("Pressure : " + this.getList()[i].pressure).append(" hPa").append("\n");
			buffer.append("Clouds : " + this.getList()[i].clouds).append(" %").append("\n");
			buffer.append("Wind : " + this.getList()[i].speed).append(" m/s").append("\n");
			buffer.append("Snow : " + this.getList()[i].snow).append("\n");
			buffer.append("Rain : " + this.getList()[i].rain).append("\n");
			buffer.append("General : " + this.getList()[i].weather[0].description).append("\n");
			buffer.append("------------------------------------------------\n");
		}
		return buffer.toString();
	}	
}
