package pl.lukaszplac123.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class WeatherConditions {
	
	private Coord coord;
	private Weather[] weather;
	private String base;
	private Main main;
	private int visibility;
	private Wind wind;
	private Clouds clouds;
	private Rain rain;
	private int dt;
	private Sys sys;
	private int id;
	private String name;
	private int cod;
	
	public Rain getRain() {
		return rain;
	}

	public void setRain(Rain rain) {
		this.rain = rain;
	}

	public Coord getCoord() {
		return coord;
	}

	public void setCoord(Coord coord) {
		this.coord = coord;
	}

	public Weather[] getWeather() {
		return weather;
	}

	public void setWeather(Weather[] weather) {
		this.weather = weather;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public Main getMain() {
		return main;
	}

	public void setMain(Main main) {
		this.main = main;
	}

	public int getVisibility() {
		return visibility;
	}

	public void setVisibility(int visibility) {
		this.visibility = visibility;
	}

	public Wind getWind() {
		return wind;
	}

	public void setWind(Wind wind) {
		this.wind = wind;
	}

	public Clouds getClouds() {
		return clouds;
	}

	public void setClouds(Clouds clouds) {
		this.clouds = clouds;
	}

	public int getDt() {
		return dt;
	}

	public void setDt(int dt) {
		this.dt = dt;
	}

	public Sys getSys() {
		return sys;
	}

	public void setSys(Sys sys) {
		this.sys = sys;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCod() {
		return cod;
	}

	public void setCod(int cod) {
		this.cod = cod;
	}

	@JsonIgnoreProperties(ignoreUnknown=true)
	public static class Coord{
		double lon;
		double lat;
		public double getLat() {
			return lat;
		}
		public void setLat(double lat) {
			this.lat = lat;
		}
		public double getLon() {
			return lon;
		}
		public void setLon(double lon) {
			this.lon = lon;
		}
	}
	
	@JsonIgnoreProperties(ignoreUnknown=true)
	public static class Weather{
		int id;
		String main;
		String description;
		String icon;
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getMain() {
			return main;
		}
		public void setMain(String main) {
			this.main = main;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String getIcon() {
			return icon;
		}
		public void setIcon(String icon) {
			this.icon = icon;
		}
	}
	
	@JsonIgnoreProperties(ignoreUnknown=true)
	public static class Main{
		double temp;
		int pressure;
		int humidity;
		double temp_min;
		double temp_max;
		public double getTemp_max() {
			return temp_max;
		}
		public void setTemp_max(double temp_max) {
			this.temp_max = temp_max;
		}
		public double getTemp() {
			return temp;
		}
		public void setTemp(double temp) {
			this.temp = temp;
		}
		public int getPressure() {
			return pressure;
		}
		public void setPressure(int pressure) {
			this.pressure = pressure;
		}
		public int getHumidity() {
			return humidity;
		}
		public void setHumidity(int humidity) {
			this.humidity = humidity;
		}
		public double getTemp_min() {
			return temp_min;
		}
		public void setTemp_min(double temp_min) {
			this.temp_min = temp_min;
		}
	
	}
	
	@JsonIgnoreProperties(ignoreUnknown=true)
	public static class Wind{
		double speed;
		int deg;
		public int getDeg() {
			return deg;
		}
		public void setDeg(int deg) {
			this.deg = deg;
		}
		public double getSpeed() {
			return speed;
		}
		public void setSpeed(double speed) {
			this.speed = speed;
		}
		
	}
	
	@JsonIgnoreProperties(ignoreUnknown=true)
	public static class Clouds{
		int all;

		public int getAll() {
			return all;
		}

		public void setAll(int all) {
			this.all = all;
		}
	}
	
	@JsonIgnoreProperties(ignoreUnknown=true)
	public static class Rain{
		String zeroh;

		public String getZeroh() {
			return zeroh;
		}

		public void setZeroh(String zeroh) {
			this.zeroh = zeroh;
		}
	}
	
	@JsonIgnoreProperties(ignoreUnknown=true)
	public static class Sys{
		int type;
		int id;
		double message;
		String country;
		long sunrise;
		long sunset;
		public int getType() {
			return type;
		}
		public void setType(int type) {
			this.type = type;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public double getMessage() {
			return message;
		}
		public void setMessage(double message) {
			this.message = message;
		}
		public String getCountry() {
			return country;
		}
		public void setCountry(String country) {
			this.country = country;
		}
		public long getSunrise() {
			return sunrise;
		}
		public void setSunrise(long sunrise) {
			this.sunrise = sunrise;
		}
		public long getSunset() {
			return sunset;
		}
		public void setSunset(long sunset) {
			this.sunset = sunset;
		}
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
			double celcius = this.getMain().getTemp() - 273.15;
			double celciusMax = this.getMain().getTemp_max() - 273.15;
			double celciusMin = this.getMain().getTemp_min() - 273.15;
			buffer.append("\n");
			buffer.append("City : " + this.getName()).append("\n");
			buffer.append("City longtitude: " + this.getCoord().getLon()).append("\n");
			buffer.append("City latitude: " + this.getCoord().getLat()).append("\n");
			buffer.append("Temperature : " + celcius).append(" stC").append("\n");
			buffer.append("Temperature max : " + celciusMax).append(" stC").append("\n");
			buffer.append("Temperature min : " + celciusMin).append(" stC").append("\n");
			buffer.append("Pressure : " + this.getMain().getPressure()).append(" hPa").append("\n");
			buffer.append("Humidity : " + this.getMain().getHumidity()).append(" %").append("\n");
			buffer.append("Visibility : " + this.getVisibility()).append(" m").append("\n");
			buffer.append("Clouds : " + this.getClouds().getAll()).append(" %").append("\n");
			buffer.append("Wind : " + this.getWind().getSpeed()).append(" m/s").append("\n");
			buffer.append("General : " + this.getWeather()[0].description).append("\n");			
		return buffer.toString();
	}
	
	public String printTemperature(){
		StringBuffer buffer = new StringBuffer();
			double celcius = this.getMain().getTemp() - 273.15;
			double celciusMax = this.getMain().getTemp_max() - 273.15;
			double celciusMin = this.getMain().getTemp_min() - 273.15;
			buffer.append("\n");
			buffer.append("City : " + this.getName()).append("\n");
			buffer.append("Temperature : " + celcius).append(" stC").append("\n");
			buffer.append("Temperature max : " + celciusMax).append(" stC").append("\n");
			buffer.append("Temperature min : " + celciusMin).append(" stC").append("\n");
		return buffer.toString();
	}
	
	public String printPressure(){
			StringBuffer buffer = new StringBuffer();
			buffer.append("\n");
			buffer.append("City : " + this.getName()).append("\n");
			buffer.append("Pressure : " + this.getMain().getPressure()).append(" hPa").append("\n");
		return buffer.toString();
	}
	
	public String printHumidity(){
			StringBuffer buffer = new StringBuffer();
			buffer.append("\n");
			buffer.append("City : " + this.getName()).append("\n");
			buffer.append("Humidity : " + this.getMain().getHumidity()).append(" %").append("\n");
		return buffer.toString();		
	}
	public String printWind(){
			StringBuffer buffer = new StringBuffer();
			buffer.append("\n");
			buffer.append("City : " + this.getName()).append("\n");
			buffer.append("Wind : " + this.getWind().getSpeed()).append(" m/s").append("\n");
		return buffer.toString();		
	}
	
}
